#!/usr/bin/env python3

"""
Draw histograms. Lots of them.
"""


from argparse import ArgumentParser
from h5py import File
import numpy as np
from pathlib import Path
import json

from matplotlib.figure import Figure

from plotting import make_purty, get_spec, add_hist

def get_args():
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('hists')
    parser.add_argument('-o', '--out-dir', type=Path,
                        default=Path('plots/hists'))
    parser.add_argument('-s', '--hist-spec', type=Path,
                        default=Path('histspec.json'))
    parser.add_argument('-e', '--ext', nargs='+', default=['.pdf'])
    return parser.parse_args()

def draw_flavor_plots(group, path, specgroup, log=False, ext='.pdf'):
    colors = {'c': 'green', 'u': 'blue', 'b': 'red'}
    fig = Figure((4,3))
    ax = fig.add_subplot()
    if group['all']['histogram'][1:-1].sum() == 0:
        print(f'skipping {path}, no data to plot')
        return
    spec = get_spec(group['all'], path.stem, specgroup)
    flavs = 'cub'
    if all(f in group for f in flavs):
        for f in flavs:
            color = colors[f]
            fhist = group[f]
            add_hist(ax, fhist, spec, label=f, color=color)
    else:
        add_hist(ax, group['all'], spec, label='all', color='k')
    make_purty(ax, spec, log)
    path.parent.mkdir(parents=True, exist_ok=True)
    fig.canvas.print_figure(path.with_suffix(ext), bbox_inches='tight')

def draw_plots(group, path, spec, ext):
    akey = 'type'
    print(f'plotting {path}')
    for name, sub in group.items():
        subspec = spec.setdefault(name, {})
        opts = dict(specgroup=subspec, ext=ext)
        if sub.attrs.get(akey) == 'flavors':
            draw_flavor_plots(sub, path/'linnear'/name, **opts)
            draw_flavor_plots(sub, path/'log'/name, log=True, **opts)
        else:
            draw_plots(sub, path/name, subspec, ext)

def run():
    args = get_args()
    if args.hist_spec.exists():
        with open(args.hist_spec) as hsfile:
            spec = json.load(hsfile)
    else:
        spec = {}
    with File(args.hists) as hfile:
        for ext in args.ext:
            draw_plots(hfile, args.out_dir, spec, ext)

    with open(args.hist_spec,'w') as hsfile:
        json.dump(spec, hsfile, indent=2)

if __name__ == '__main__':
    run()
