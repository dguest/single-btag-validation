#!/usr/bin/env python3

"""
build plots of everything
"""

from argparse import ArgumentParser
from h5py import File, Group, Dataset
import numpy as np
from pathlib import Path
import json
import math
import warnings
import sys

from histograms import Histogram, FloatHistogram, IntHistogram, DatasetHists
from batching import yield_batches


def get_args():
    parser = ArgumentParser(description=__doc__)
    d = dict(help='default: %(default)s')
    parser.add_argument('inputs', nargs='+', type=Path)
    parser.add_argument(
        '-o',
        '--output',
        type=Path,
        default=Path('hists.h5'),
        **d,
    )

    ranges = parser.add_mutually_exclusive_group()
    ranges.add_argument(
        '-r',
        '--ranges',
        type=Path,
        default=Path('ranges.json'))
    ranges.add_argument(
        '-a',
        '--append-ranges',
        nargs='?',
        type=Path,
        const=Path('ranges.json'))

    parser.add_argument(
        '-m',
        '--max-entries',
        nargs='?',
        const=100,
        type=int,
        help='with no args: %(const)s',
    )
    parser.add_argument(
        '-s', '--strict', action='store_true',
        help='crash on warnings'
    )
    return parser.parse_args()


def get_dataset_ranges(ds, oldranges, number=10_000):
    if ds.ndim > 2:
        raise ValueError("can't use a {} dimensional input".format(ds.ndims))
    # read a bit less for 2d arrays, since they probably have around
    # 20 constituents per jet
    if ds.ndim == 2:
        number = math.ceil(number / (ds.shape[1] / 2))
    sample = ds[0:number]
    if ds.ndim == 2:
        sample = sample[sample['valid']]
    ranges = {}
    for name, tp in sample.dtype.descr:
        if name in oldranges:
            continue
        if name == 'valid':
            continue
        data = sample[name]
        if ds.ndim == 1:
            data = data[np.isfinite(data)]
        rtype = {'b':'bool','i':'int', 'u':'int','f':'float'}[
            np.dtype(tp).kind]
        # override cases where they look like integers that were
        # stored as floats
        if (data.astype(int) == data).all():
            rtype = 'int'
        # now build the output dictionary
        typerange = {
            'type': rtype,
        }
        if rtype == 'int':
            typerange['range'] = (int(data.min())-1, int(data.max())+1)
        elif rtype == 'float':
            center = (data.min() + data.max()) / 2
            halfspan = data.ptp() / 2
            low = center - halfspan * 1.1
            high = center + halfspan * 1.1
            typerange['range'] = (low.item(), high.item())
        ranges[name] = typerange
    return ranges


def get_ranges(group, ranges):
    new = {}
    for name, sub in group.items():
        subrange = ranges.get(name, {})
        if isinstance(sub, Group):
            new[name] = {
                'type': 'group',
                'items': get_ranges(sub, subrange)
            }
        else:
            new[name] = {
                'type': 'dataset',
                'items': get_dataset_ranges(sub, subrange)
            }
    return new


def get_hists(group):
    hists = {}
    for name, sub in group.items():
        tp = sub['type']
        if tp == 'group':
            hists[name] = get_hists(sub['items'])
        elif tp == 'dataset':
            features = DatasetHists()
            # skip flavor stuff for eventwise
            flavors = ['all'] + ([] if name == 'eventwise' else list('cub'))
            for flav in flavors:
                ffeat = {}
                for fname, spec in sub['items'].items():
                    hrange = spec['range']
                    if not np.isfinite(hrange).all():
                        warnings.warn(
                            f'{fname} range is {hrange}, skipping',
                            stacklevel=2
                        )
                        continue
                    if spec['type'] == 'float':
                        ffeat[fname] = FloatHistogram(fname, hrange)
                    elif spec['type'] == 'int':
                        ffeat[fname] = IntHistogram(fname, hrange)
                    else:
                        print(f"skipping {fname}: type {spec['type']}")
                features[flav] = ffeat
            hists[name] = features
    return hists


def fill_hists(group, hists, max_entries=None):
    flavmap = {'b': 5, 'c': 4, 'u': 0}
    label = 'HadronConeExclTruthLabelID'
    for name, sub in hists.items():
        if not name in group:
            raise ValueError('no {} found in: {}'.format(
                name, ', '.join(group.keys())))
        subgroup = group[name]
        if not isinstance(sub, DatasetHists):
            fill_hists(subgroup, sub, max_entries)
        else:
            if not isinstance(subgroup, Dataset):
                raise ValueError(f'{name} is not a dataset')
            jet_iter = yield_batches(
                group['jets'],
                fields={label},
                max_entries=max_entries
            )
            other_iter = yield_batches(
                subgroup,
                max_entries=max_entries,
            )
            print(f'filling {name}', end='')
            for jbatch, batch in zip(jet_iter, other_iter):
                print('.', end='', flush=sys.stdout.isatty())
                for flav, features in sub.items():
                    if flav == 'all':
                        fbatch = batch
                    else:
                        fbatch = batch[jbatch[label] == flavmap[flav]]
                    if fbatch.ndim == 2:
                        fbatch = fbatch[fbatch['valid']]
                    for hist in features.values():
                        hist.fill(fbatch)
            print('done')


def write_hists(group, output):
    for name, sub in group.items():
        if isinstance(sub, Histogram):
            output.attrs['type'] = 'flavors'
            sub.write(output, name)
        else:
            subout = output.create_group(name)

            write_hists(sub, subout)


def rearrange_flavors(group):
    """Swap A/flav/B --> A/B/flav

    It's more convenient on the plotting side to cluster by flavor, so
    we do here.

    """

    if isinstance(group, DatasetHists):
        nodes = {}
        for name, sub in group.items():
            for subname, subsub in sub.items():
                nodes[name, subname] = subsub
        group.clear()
        for (name, subname), val in nodes.items():
            group.setdefault(subname, {})[name] = val
        return
    for name, sub in group.items():
        if not isinstance(sub, Histogram):
            rearrange_flavors(sub)


def run():
    args = get_args()
    if args.strict:
        warnings.simplefilter('error')
    old_range_path = args.append_ranges or args.ranges
    if old_range_path.exists():
        with open(old_range_path) as rfile:
            old_ranges = json.load(rfile)
    else:
        old_ranges = {}
    if not old_ranges or args.append_ranges:
        with File(args.inputs[0]) as h5file:
            ranges = get_ranges(h5file, old_ranges)
        with open(old_range_path,'w') as rfile:
            json.dump(ranges, rfile, indent=2)
    else:
        ranges = old_ranges

    hists = get_hists(ranges)

    for fpath in args.inputs:
        print(f'filling from {fpath}')
        with File(fpath) as hfile:
            fill_hists(hfile, hists, max_entries=args.max_entries)

    rearrange_flavors(hists)

    args.output.unlink(missing_ok=True)
    with File(args.output, 'w') as hout:
        write_hists(hists, hout)

if __name__ == '__main__':
    run()
