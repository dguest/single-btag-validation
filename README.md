Single b-Tagging Validation Scripts
===================================

This repository contains some very simple scripts to plot the outputs
from the [`training-dataset-dumper`][tdd]. They are specific to the
trigger outputs.

General principles:

- The code is split between _aggregating_ the data (from the upstream dumpster jobs), and _plotting_.
- Aggregation jobs should have minimal dependencies: python3 + h5py
- Plotting jobs require matplotlib (which pulls in a lot more stuff)
  but minimal other dependencies.


Quick Start for ROC curves
--------------------------

- Run `btag_make_1d_rocs.py dumper-output.h5` to make some histograms
- Run `btag_draw_1d_rocs.py roc1d.h5` to make plots

Quick start for other plots
---------------------------

- Run `btag_make_1d_hists.py dumper-output.h5` to make histograms.
- This will also generate a `ranges.json` file that specifies the
  ranges of all the histograms you'll plot. This is only generated if
  you don't have one: you can tweak it manually too.
- Run `btag_draw_1d_hists.py hists.h5` to draw all the histograms.

Notes on file structure
-----------------------

### Input datasets ###

In the datasets coming from the dumpster, we assume that a dataset
called `jets` will exist on the same level as other datasets (this is
used for truth labelling). Otherwise the input files can be
arbitrarily nested.

### Histogram files ###

The histogram files will included nested groups mirroring the
structure of the input datasets. There are a few special group
attributes that were added to make plotting easier, all strings under
the key `type`:

- Groups with `type` equal to `float` or `int` are histograms, they
  contain one `histogram` dataset giving the bin counts, and some
  other datasets giving ranges etc.

- Groups a `type` equal to `flavors` contain one histogram for each
  flavor.

Making a website
----------------

You can run `btag_make_website.py plots` to build a website showing
all your plots.

[tdd]: https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper/-/tree/r22

