#!/usr/bin/env python3

"""
Draw roc curves
"""


from argparse import ArgumentParser
from h5py import File
import numpy as np
from pathlib import Path

from matplotlib.figure import Figure

def get_args():
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('discrim_hists')
    parser.add_argument('-o', '--out-dir', type=Path, default='plots/roc')
    return parser.parse_args()


nmap = {
    'DL1r':'Trigger DL1r',
    'dipsLoose20210517':'FastDIPS',
    'dipsLooseSimpleIp20210517':'FasterDIPS',
    'OfflineMatchedDL1r':'Offline DL1r',
    'DL1dv00': 'DL1d'
}

def dr_annotate(val):
    return r' $\Delta R < ' + f'{val}' + r'$'

def run():
    args = get_args()

    hists = {}
    annotations = {}
    with File(args.discrim_hists,'r') as roc_file:
        for tagger, flav_group in roc_file.items():
            flav_hists = hists.setdefault(tagger,{})
            annotations[tagger] = dict(flav_group.attrs.items())
            for flav, hist in flav_group.items():
                flav_hists[flav] = np.asarray(hist)

    draw_rocs(hists, args.out_dir, annotations)

def draw_rocs(flav_hists, out_dir, annotations):

    fig = Figure((4,3))
    ax = fig.add_subplot()

    for tagger, flavs in flav_hists.items():
        beff = flavs['b'][::-1].cumsum() / flavs['b'].sum()
        leff = flavs['u'][::-1].cumsum() / flavs['u'].sum()
        valid = (beff > 0.5) & (leff > 0)
        rej = 1/leff[valid]
        eff = beff[valid]
        label=nmap[tagger]
        dr_match = annotations[tagger].get('dR_match')
        if dr_match:
            label += dr_annotate(dr_match)
        ax.plot(eff, rej, label=label)

    ax.legend(frameon=False)
    ax.set_yscale('log')
    ax.set_xlabel(r'$b$ Efficiency')
    ax.set_ylabel(r'Light jet rejection')

    out_dir.mkdir(parents=True, exist_ok=True)
    fig.canvas.print_figure(out_dir / 'test.pdf', bbox_inches='tight')

if __name__ == "__main__":
    run()
