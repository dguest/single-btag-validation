import numpy as np

def get_spec(group, name, specgroup):
    if name in specgroup:
        return specgroup[name]
    tp = group.attrs['type']
    xkey = 'edges' if tp == 'float' else 'values'
    low, high = [group[xkey][x] for x in [0,-1]]
    units = ''
    mult = 1.0
    if high - low > 1e4 and tp == 'float':
        units = 'GeV'
        mult = 1e-3
    spec = dict(
        xlabel=name,
        low=low.item(),
        high=high.item(),
        units=units,
        mult=mult
    )
    specgroup[name] = spec
    return spec

def add_hist(ax, fhist, spec, **args):
    vals = fhist['histogram'][1:-1]
    if fhist.attrs['type'] == 'float':
        edges = np.asarray(fhist['edges']) * spec['mult']
        ax.stairs(vals, edges, **args)
    elif fhist.attrs['type'] == 'int':
        midpoints = np.asarray(fhist['values'])
        ax.step(midpoints, vals, where='mid', **args)

def make_purty(ax, spec, log=False):
    ax.legend(frameon=False)
    xlab = spec['xlabel']
    units = spec['units']
    xlab_pretty = f'{xlab} [{units}]' if units else xlab
    ax.set_xlabel(xlab_pretty)
    m = spec['mult']
    ax.set_xlim(spec['low']*m, spec['high']*m)
    if log:
        ax.set_yscale('log')
    else:
        ax.set_ylim(bottom=0)
